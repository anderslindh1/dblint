from __future__ import annotations

import argparse
from pathlib import Path
from typing import Sequence

from lark import Lark
from lark import Token


def lint_db(s):
    parser = Lark.open(Path(__file__).parent / "grammar.lark", propagate_positions=True)
    tree = parser.parse(s)
    all_tokens = tree.scan_values(lambda v: isinstance(v, Token))
    for token in all_tokens:
        print(f"{token.line}:{token.end_line} ({token.column}:{token.end_column}) {token}")
    print(tree.pretty())


def loads(s) -> str:
    return lint_db(s)


def load(fp) -> str:
    return loads(fp.read())


def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", nargs="*", help="filenames to check")
    args = parser.parse_args()

    retval = 0
    for filename in args.filenames:
        with open(filename, "r") as f:
            try:
                load(f)
            except Exception as exc:
                print(f"{filename}: {exc}")
                retval = 1
    return retval


if __name__ == "__main__":
    raise SystemExit(main())
